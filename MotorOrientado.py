#coding: utf-8
from Banco.MyBanco import GerenciaBanco
from Banco.VariavelBanco import VariavelBanco
from base_de_regras import BaseDeRegras
from mem_de_fatos import MemDeFatos

from fato import Fato
import re
from regra import Regra

class MotorDeInferencia(object):
    def __init__(self,banco=VariavelBanco()):
        self.banco_var=banco
        self.mem_fatos=MemDeFatos()

        self.base=BaseDeRegras()
    def executa(self,fato):

        if (self.mem_fatos.rec_memFatos(fato)):
            return self.mem_fatos.rec_memFatos(fato) # Retorna o valor do fato na memoria de fatos
        else:
            regras=self.base.busca_regras(fato) #se tiver vazia = falso, pois nao possui regras
            if regras:
                for regra in regras: #Percorre todas as regras que podem provar o fato desejado.
                    # print 'encontrou regras'
                    simb=False
                    v1=None
                    v2=None
                    negacao=False

                    #[A,&,not,B]
                    for p in regra.quebra_regra(): #Quebra a regra em uma lista onde cada componente da regra vira um atomo, pra ser processado individualmente
                        # print 'processando: %s'%p
                        if p!= '&' and p!='or' and p!='not':


                            temp=self.executa(p)
                            v1=Fato()
                            v1.recebe_fato(temp)


                            v1.verifica_verdade(p)
                            v1.recebe_negacao(negacao)#Nega ou nao o valor verdade do FATO

                            #print "Memoria de Fatos atualizada"
                            #print self.mem_fatos.exibir()



                            v2=self.processamento_logico(v1,v2,simb)


                            negacao=False
                        elif p=='not':
                            negacao=True
                        else:
                            simb=p

                    v2.fator_de_confianca=self.fator_conclusao(v2.fator_de_confianca,regra.fc)


                    v2.tipo=self.banco_var.busca_variavel(fato)

                    v2.valor=regra.get_conclusao_or_parametro(2)




                    self.mem_fatos.add(v2,fato)
                return self.mem_fatos.rec_memFatos(fato)
                    #Se o fato nao tiver na memoria de fatos
                    #   faca calculo simples
                    #senao
                    #   Faca o calculo utilizando o fator corrente e o fator atual na memoria de fatos

            else:#Zona do codigo destinada para uso do usuario, respondendo perguntas feitas pelo sistema
                ft=self.get_fato_or_valor(fato,1)
                valor=raw_input("qual valor de %s? "%ft)
                while(True):
                    try:
                        fc=input("qual Fator de confianca para %s=%s? "%(ft,valor))
                        break
                    except:
                        print 'insira um numero real ou inteiro, ex 15.5'
                fatoC=Fato(fator=fc)
                fatoC.add_valor_usuario(valor,fato)
                fatoC.tipo=self.banco_var.busca_variavel(fato)
                self.mem_fatos.add(fatoC,fato)

                return fatoC
    #Fator de conclusao de uma regra
    def fator_conclusao(self,f_condicoes,f_regra):

        resultado=(f_condicoes/100.0 * f_regra/100.0)*100


        return resultado
    #Processamento logico de duas premissas
    def processamento_logico(self,p1, p2, simb):


        gc=self.grau_de_certeza(p1,p2,simb)
        v3=Fato(parametro=False,fator=gc)


        if simb == 'or':
            v3.parametro = p1.parametro or p2.parametro
        elif simb == '&':
            v3.parametro = p1.parametro and p2.parametro

        else:
            v3.parametro = p1.parametro

        return v3
    def iniciar(self):
        self.executa('livro')
        self.recuperaMelhor_livro()
    #Calcula o grau de certeza de duas premissas
    #validado
    def grau_de_certeza(self,p1,p2,simb):


        if simb=='&':
            # print "Conjuncao"
            valor=(p1.fator_de_confianca/100.0*p2.fator_de_confianca/100.0)*100.0
        elif simb =='or':
            # print "Disjuncao"
            valor=(p1.fator_de_confianca/100.0+p2.fator_de_confianca/100.0-p1.fator_de_confianca/100.0*p2.fator_de_confianca/100.0)*100


        else:
            # print "Sem Simbolos"
            valor=p1.fator_de_confianca

        return valor

    def get_fato_or_valor(self,fato,num):


        p_quebra=re.match(r'^(\w+)\s*=?\s*(\w*)$',fato)
        return p_quebra.group(num)

    def recuperaMelhor_livro(self):
        print 'Livros Recomendados'
        lista=self.mem_fatos.memoria['livro']
        lista2=[]
        for livro in lista:
            if livro.parametro == True:
                lista2.append(livro)

        # if lista[0].parametro==False:
        #     lista.pop(0)

        lista2.sort(key=lambda a: a.fator_de_confianca)

        for livro in lista2[::-1]:

            print 'livro: %s com fator de confianca: %s parametro: %s'%(livro.valor,livro.fator_de_confianca,livro.parametro)
        if len(lista2)==0:
            print 'Infelizmente não foi possível encontrar nenhum livro com essas características.'
# motor=MotorDeInferencia()
# motor.executa('livro')
# motor.mem_fatos.exibir()

#1-Refatorar o codigo
#2-Buscar pelo tipo dos fatos na base de dados
#3-Quebrar os fatos com foco em chave e valor
#4-Reestruturar a selecao na memoria de fatos, quando for selecionar o tipo do dado que sera inserido.
#5-Fazer um validador de regras - Feito
#6-Criar as varias cenas do sistema Uso do motor, criacao de regras, criacao de variaveis, etc.

lista=[Fato(1,1,1,1),Fato(1,1,1,1),Fato(1,1,1,1)]
l2=lista

print lista.index(lista[0])