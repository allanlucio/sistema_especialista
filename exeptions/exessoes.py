__author__ = 'allan'

class sintaxeException(Exception):
    pass

class varNotExistException(Exception):
    pass

class DadoJaCadastradoException(Exception):
    pass

class ErroNaConsultaException(Exception):
    pass