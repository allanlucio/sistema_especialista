
import tkMessageBox
from Banco.MyBanco import GerenciaBanco
from Banco.RegraBanco import RegraBanco
from Banco.VariavelBanco import VariavelBanco
from exeptions.exessoes import sintaxeException, varNotExistException, DadoJaCadastradoException
from regra import Regra


__author__ = 'lapec'
import re

from re import compile

#Quebra as regras para que seja feita a analise de cadastro no banco
def quebra_regra(regra):

    re2 = re.findall(r'([a-zA-Z]+\s*=\s*[a-zA-Z]+)', regra)

    # print re2
    # re2.pop(0)
    # var = False
    # while (not var):
    #     try:
    #         var = re2.remove(None)
    #
    #     except:
    #         var = True
    # var=False
    # while (not var):
    #     try:
    #         var = re2.remove('')
    #     except:
    #         var = True
    # while (not var):
    #     try:
    #         var = re2.remove(r'\s*not\s*')
    #     except:
    #         var = True




    return re2

#print quebra_regra('se revisao=true or not exercicio=false & disciplina=banco entao livro=loucura')

#Valida a funcao sintaticamente
def valida_regra_sintaxe(regra):
    regular_ex=r"^se\s+(not\s+)?[a-zA-Z]+(\s*=\s*[a-zA-Z]+)?(\s+((or)|&)\s+(not\s+)?[a-zA-Z]+(\s*=\s*[a-zA-Z]+)?)*\s+entao\s+[a-zA-Z]+(\s*=\s*[a-zA-Z]+)?$"
    regex = compile(regular_ex)



    r = regex.match(regra)

    if not bool(r):
        raise sintaxeException("Erro de sintaxe, verifique a regra.")

#Quebra um fato, se o mesmo tiver igualdade e senao mantem como antes
#Preenche um vetor de duas posicoes, posicao 0 com o nome da variavel e posicao 1 com o tipo dela
#1 Univalorado
#2 Multivalorado
def quebra_igualdade(fato):

    if '=' in fato:
        re2=re.split(r'\s*=\s*',fato)
        re2[1]=1
    else:
        re2=[fato,2]

    return re2

#Visa validar cada fato, verificando se existe na base de dados das variaveis e se o tipo condiz com o cadastrado.
def valida_fatos_Database(regra):
    gerb = VariavelBanco()
    erros=[]
    for fato in quebra_regra(regra):
        resultado=quebra_igualdade(fato)#[exercicio,1]
        query=gerb.busca_variavel(resultado[0])

        if query == None:
            erros.append("A Variavel > %s < nao esta cadastrada na nossa base de dados!"%(resultado[0]))

        # elif not query==resultado[1]:
        #     erros.append("Verifique o tipo do fato/premissa %s"%(resultado[0]))


    if erros:
        raise varNotExistException(erros)

    print "Variaveis validadas!!"

def valida_regra(regra):
    print '\n\nValidando Regra\n\n'
    try:
        valida_regra_sintaxe(regra.regra)
        valida_fatos_Database(regra.regra)
        banco = RegraBanco()
        banco.inserir([regra])

    except sintaxeException as e2:
        print e2.message
    except varNotExistException as e:
        lista=list(e.message)
        while (lista):
            print lista.pop(0)
    except DadoJaCadastradoException as e3:
        print e3.message


#valida_fatos_Database(regra = 'se nublado=aaa & asssss & ffff & bbbb & cccc or sdasdasd entao chove = pouco')

#valida_regra_sintaxe(regra = 'se nublado=aaa & asssss & ffff & bbbb & cccc or sdasdasd entao chove = pouco')



#gerb.busca_variavel()




# while True:
#     print varb
#     regra=raw_input("Escreva uma regra!")
#     if regra =='exit':
#         break
#
#     fc=int(raw_input("Qual fator de confianca?"))
#     valida_regra(Regra(regra=regra,fc=fc))

#valida_regra(Regra(regra='se revisao=true or not exercicio=false & disciplina=banco entao livro=loucura',fc=10))