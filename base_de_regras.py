from time import sleep
from Banco import MyBanco
from Banco.MyBanco import GerenciaBanco
from Banco.RegraBanco import RegraBanco
from regra import Regra
import re
__author__ = 'allan'
import re
base_de_regras2=[
    'se A entao B',
    'se A entao C',
    'se B & C entao D',
    'se D or A entao E',
    'se B entao F'


]
base_de_regras=[
    'se nublado entao chove',
    'se chove & descoberto entao molha',
    'se molha entao gripa',
    'se sereno entao gripa'



]
# def busca_regras(fato):
#     fato=re.match(r'^(\w+)\s*=?\s*\w*$',fato).group(1)
#
#
#
#     lista=[]
#     banco = GerenciaBanco()
#
#     base_de_regras=banco.selecionar()
#     print base_de_regras
#     for a in base_de_regras:
#
#         re1 = re.match(r'.* entao (\w+)', a[0])
#
#         if (re1.group(1)==fato):
#
#             lista.append(Regra(a[0],a[1]))
#     return lista
#
# print busca_regras('Z')


class BaseDeRegras(object):
    def __init__(self):
        self.banco=RegraBanco()
        self.base=self.banco.selecionar() #Todas as regras

    def busca_regras(self,fato):
        fato=re.match(r'^(\w+)\s*=?\s*\w*$',fato).group(1)
        lista=[]
        for a in self.base:


            re1 = re.match(r'.* entao (\w+)', a[1])

            if (re1.group(1)==fato):

                lista.append(Regra(a[1],a[2],a[0]))
        return lista