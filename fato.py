from time import sleep

__author__ = 'allan'
import re
class Fato(object):
    parametro=""
    valor=False
    fator_de_confianca=0
    tipo=""

    #constantes
    MULTIVALORADO=1
    MONOVALORADO=2

    #se A entao Livro=Logica

    def __init__(self, valor=False,fator=0,tipo=MONOVALORADO,parametro=False):

        self.valor=valor
        self.fator_de_confianca=fator
        self.tipo=tipo
        self.parametro=parametro

    #Adiciona o valor passado pelo usuario e verifica se a premissa recebe valor verdade True/false
    def add_valor(self,valor,premissa):

        try:

            p_quebra=re.match(r'^(\w+)\s*=\s*(\w+)$',premissa)
            p_quebra=p_quebra.groups()
            if p_quebra[1]==valor:#Verifica se o parametro dado pelo usuario prova o valor verdade do fato.
                self.valor=True
                self.parametro=p_quebra[1]
            elif p_quebra[1] !=valor:
                self.valor=False
                self.parametro=p_quebra[1]
        except:#Variavel simples, nao possui igualdade

            self.valor=valor.lower() in ['sim','yes','verdade','true','s','v','y','ok']
            self.parametro=valor

    def add_valor_usuario(self,valor,premissa):

        self.parametro=valor
        self.valor=valor

    def verifica_verdade(self,fato):
        p_quebra=re.match(r'^(\w+)\s*=\s*(\w+)$',fato)
        try:
            if p_quebra.group(2)==self.valor:
                self.parametro=True
            else:
                self.parametro=False
        except:
            print 'nao precisou checar'




    def recebe_negacao(self,negacao):
        if negacao:
            self.parametro=not self.parametro

    def recebe_fato(self,fato):
        self.valor=fato.valor
        self.parametro=fato.parametro
        self.fator_de_confianca=fato.fator_de_confianca
        self.tipo=fato.tipo

    def __str__(self):
        return "%s : %s : %s"%(self.valor,self.fator_de_confianca,self.parametro)

    def __unicode__(self):
        return " %s : %s"%(self.valor,self.fator_de_confianca)









