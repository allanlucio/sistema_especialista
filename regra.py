__author__ = 'lapec'
import re
class Regra(object):
    def __init__(self,regra,fc,id=-1):
        self.regra=regra
        self.fc=fc
        self.id=id
    def quebra_regra(self):

        re2 = re.findall(r'or|not|&|[a-zA-Z]+\s*=\s*[a-zA-Z]+', self.regra)
        re2.pop(-1)

        # re2.pop(0)
        # var = False
        # while (not var):
        #     try:
        #         var = re2.remove(None)
        #     except:
        #         var = True
        # var=False
        # while (not var):
        #     try:
        #         var = re2.remove('')
        #     except:
        #         var = True
        # re2.remove('entao')
        # re2.pop()
        # print re2

        return re2
    def le_conclusaoRegra(self):
        re2 = re.split(r".+ entao (\w+=\w+)", self.regra)
        re2.remove('')
        re2.remove('')
        return re2[0]
    #Se num =1 retorna conclusao
    #Se num=2 retorna parametro
    def get_conclusao_or_parametro(self,num):

        conclusao=self.le_conclusaoRegra()
        p_quebra=re.match(r'^(\w+)\s*=?\s*(\w*)$',conclusao)
        return p_quebra.group(num)


