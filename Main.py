#coding: utf-8
from Banco.RegraBanco import RegraBanco
from Banco.VariavelBanco import VariavelBanco
from MotorOrientado import MotorDeInferencia
from regra import Regra
from verificaRegra import valida_regra


class Main(object):
    def executa(self):
        while(True):
            print '1: Motor de Inferência\n2: Gerênciamento de regras.\n3:Gerênciamento de variaveis\n4: Instruções\n9: Sair'
            opc=raw_input('Digite a opção desejada')
            if opc=='1':
                self.buscaMotor()
            elif opc=='2':
                self.regras()
            elif opc=='3':
                self.gerencia_variaveis()
            elif opc=='4':
                self.intrucoes()
            elif opc=='9':
                print('Obrigado por usar nosso Motor de Inferência!')
                break

    def buscaMotor(self):
        while(True):
            mt=MotorDeInferencia()
            mt.iniciar()
            if(raw_input('deseja sair? s ou n').lower()=='s'):
                break
    #Opção 2 gerenciamento de regras
    def regras(self):
        while(True):
            print'1: Inserir Regra.\n2: Deletar regra\n3: Ver Regras.\n9: Sair.'
            opc=raw_input('Digite a opção desejada...')
            if opc=='1':
                self.inserirRegra()
            elif opc=='2':
                self.deleta_regra()
            elif opc =='3':
                self.listar_regras()
            elif opc=='9':
                break
            pass

    def inserirRegra(self):
        while(True):
            print 'As variaveis cadastradas estão listadas abaixo:'
            VariavelBanco().selecionar()
            regra=raw_input('Digite a Regra...')
            try:
                fc=int(raw_input('Digite o Fator de Confianca'))
                valida_regra(Regra(regra,fc))
            except:
                print 'Insira o fator de confiança por favor!'
                print 'Caso tenha duvidas verifique as instrucoes no menu inicial..'
                pass

            if(raw_input('deseja sair? s ou n').lower()=='s'):
                break


    def deleta_regra(self):
        while(True):
            rb=RegraBanco()
            lista=rb.selecionar()
            if len(lista)==0:
                print 'Base de Regras Vazia..'
                raw_input('\nAperte qualquer tecla para continuar...')
                print '\n'

                break
            for a in lista:
                print 'id: %s  regra:%s'%(a[0],a[1])
            rb.deletar(raw_input('Diga o Id da regra que deseja remover'))

            if(raw_input('deseja sair? s ou n').lower()=='s'):
                    break

    def listar_regras(self):
        re=RegraBanco()
        print 'Lista de Regras\n'
        for a in re.selecionar():
            print 'Regra: %s  [ID= %s e F. de Confianca= %s]'%(a[1],a[0],a[2])
        raw_input('\nAperte qualquer tecla para continuar...')
        print '\n'
    #gerenciamento de variaveis
    def gerencia_variaveis(self):
        while(True):
            print'1: Inserir Variavel.\n2: Deletar Variavel\n3: Ver Variaveis.\n9: Sair.'
            opc=raw_input('Digite a opção desejada...')
            if opc=='1':
                self.inserirVariavel()
            elif opc=='2':
                self.deletaVariavel()
            elif opc =='3':
                self.listarVariaveis()
            elif opc=='9':
                break
            pass

    def inserirVariavel(self):
        vb=VariavelBanco()
        try:
            nome=raw_input('Qual nome da variável?')
            print 'Tipos: Monovalorado=2 e Multivalorado=1'
            tipo=input("Qual o tipo? Obs: Digite 1 ou 2 seguindo o descrito na linha a cima.")
            vb.inserir(nome,tipo)
        except:
            print 'Verifique os dados informados, algo está errado.'
        raw_input('\nAperte qualquer tecla para continuar...')
        print '\n'
    def deletaVariavel(self):
        vb=VariavelBanco()
        print '\t\t\tVariaveis:'
        for var in vb.base_select():
            print 'codigo: %s Nome: %s tipo: %s'%(var[0],var[1],var[2])
        try:
            vb.deletar(input('Diga qual variavel deseja remover informando o id dela'))
        except:
            print 'Por favor, insira uma das chaves'
            self.deletaVariavel()
        raw_input('\nAperte qualquer tecla para continuar...')
        print '\n'
    def listarVariaveis(self):
        print '\t\t\tLista de Variaveis:'
        vb=VariavelBanco()
        vb.selecionar()
        raw_input('\nAperte qualquer tecla para continuar...')
        print '\n'

    def intrucoes(self):
        print '\n\n'
        print 'Normas de Uso:\n'
        print '1:Esse sistema não comporta precedência com uso de parênteses nas expressões lógicas.'
        print '2:A Ordem lógica da execução das premissas é da esquerda pra direita, isso influência na conclusão.'
        print '3:As Premissas apenas aceitam variáveis monovaloradas'
        print '4:Conclusões podem ser monovaloradas ou multivaloradas'
        print '5:APENAS Variaveis cadastradas na base de dados poderão ser utilizadas na construção de novas regras.'
        print '6:Padrão de codificação ASCII'
        print '7:Nome e valor das variaveis não podem conter espacos.\nEx:\nlivro=meu exemplo  << Errado\nlivro=meuexemplo << Correto'
        raw_input('\nAperte qualquer tecla para continuar...')
        print '\n'


        print '\n1:Sintaxe da Regra:\n'
        print '2:Toda regra começa com a cadeia: se'
        print '3:Conjunções são denotadas pelo símbolo: &'
        print '4:Disjunções são denotadas pelo símbolo: or'
        print '5:Negação é denotada pela cadeia: not'
        print '6:Conclusão deve ser precedida de: entao'
        print '7:Exemplo:\nse exercicio=true & revisao=false or not introducao=true & disciplina=banco entao livro=livroexemplo '
        raw_input('\nAperte qualquer tecla para continuar...')
        print '\n'

Main().executa()