from time import sleep
import re
from Banco.MyBanco import GerenciaBanco
from Banco.VariavelBanco import VariavelBanco
from fato import Fato

__author__ = 'allan'

class MemDeFatos(object):
    memoria={}
    banco=None

    def __init__(self):
        self.memoria={}
        self.banco=VariavelBanco()
    #Adiciona na memoria de fatos
    def add(self,fato,chave):
        chave=self.get_conclusao_or_parametro(chave,1)

        if Fato.MONOVALORADO == fato.tipo:
            self.inserir_fato_Monovalorado(fato,chave)
        elif Fato.MULTIVALORADO == fato.tipo:
            self.inserir_fato_Multivalorado(fato,chave)


    def inserir_fato_Monovalorado(self,fato,chave):
        existe=self.rec_memFatos(chave)
        if existe:
            self.memoria[chave].fator_de_confianca=self.fator_conclusao_varias_regras(existe.fator_de_confianca,fato.fator_de_confianca)
        else:

            self.memoria[chave]=fato


    #Insere um fato Multivalorado, diferenciando quando ja existe e quando e necessario recalcular o fator de confianca
    def inserir_fato_Multivalorado(self,fato,chave):
        existe=self.busca_fato_multivalorado(chave,fato.valor)
        if existe == None:
            self.memoria[chave]=[]
            self.memoria[chave].append(fato)
        elif existe == False:
            self.memoria[chave].append(fato)
        else:
            existe.fator_de_confianca=self.fator_conclusao_varias_regras(existe.fator_de_confianca,fato.fator_de_confianca)



    #Retorna um fato baseado na chave e no parametro do fato
    #Retorno
    #   None = inexistencia desse fato na memoria
    #   fato = quando foi encontrado na memoria
    #   false = existe referencia para o fato, mas nao com o mesmo valor.
    def busca_fato_multivalorado(self,chave,valor):
        if not self.is_in_memory(chave):
            return None
        for fato in self.memoria[chave]:
            if fato.valor==valor:


                return fato
        return False
    #Verifica se o fato existe na memoria e retorna um booleano informando o valor verdade.
    def is_in_memory(self,chave):
        chave=self.get_conclusao_or_parametro(chave,1)
        try:
            self.memoria[chave]
            return True
        except:
            return False
    #Recupera um fato na memoria de fatos, seja ele mono ou multivalorado
    def rec_memFatos(self,chave):
        chave=self.get_conclusao_or_parametro(chave,1)
        valor=self.get_conclusao_or_parametro(chave,2)
        tipo=self.banco.busca_variavel(chave)

        try:
            if tipo==Fato.MONOVALORADO:
                return self.memoria[chave]
            elif tipo==Fato.MULTIVALORADO:
                return self.busca_fato_multivalorado(chave,valor)

        except:
            return False


    def fator_conclusao_varias_regras(self,f_previo,f_regra_corrente):

        print f_previo,f_regra_corrente
        print (f_previo + f_regra_corrente*(100-f_previo)/100.0)

        return (f_previo + f_regra_corrente*(100-f_previo)/100.0)

    def exibir(self):
        for a in self.memoria.keys():
            if type(self.memoria[a])==type([]):
                for b in self.memoria[a]:
                    print "\t %s : %s"%(a,b.__str__())
            else:
                print "%s : %s"%(a,self.memoria[a].__str__())

    def get_conclusao_or_parametro(self,chave,num):

        conclusao=chave
        p_quebra=re.match(r'^(\w+)\s*=?\s*(\w*)$',conclusao)
        return p_quebra.group(num)