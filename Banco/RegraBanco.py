from Banco.MyBanco import GerenciaBanco
from exeptions.exessoes import DadoJaCadastradoException
from regra import Regra

__author__ = 'allan'

class RegraBanco(GerenciaBanco):
    tabela='regrab'

    def __init__(self):
        try:

            self.executaUpdate('Create Table regrab (id INTEGER PRIMARY KEY autoincrement,regra text unique,fc FLOAT not NULL DEFAULT 100)')

        except:
            pass

    def selecionar(self,codigo='select * from Regrab;'):
        return self.executaQuery(codigo)
    def inserir(self,regras):
        for regra in regras:
            print "insert into regrab(regra,fc) values(%s%s%s,%s%f%s)"%("'",regra.regra,"'","'",regra.fc,"'")
            #print regra
            try:
                self.executaUpdate("insert into regrab(regra,fc) values(%s%s%s,%s%f%s)"%("'",regra.regra,"'","'",regra.fc,"'"))
                print '\nRegra inserida com Sucesso!\n'
            except DadoJaCadastradoException:
                print 'Essa Regra ja foi cadastrada'
            except:
                print 'Algum erro inesperado aconteceu'
    def deletar(self,id):
        codigo="delete from regrab where id=%s%s%s"%("'",id,"'")
        self.executaUpdate(codigo)

# a =RegraBanco()
# print a.inserir([Regra('se introducao=true entao livro=tesste',12)])
# print a.selecionar()