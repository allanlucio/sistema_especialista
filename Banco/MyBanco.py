from time import sleep
from exeptions.exessoes import DadoJaCadastradoException, ErroNaConsultaException
from regra import Regra
import re
import os

import sqlite3
class GerenciaBanco(object):
    caminho=os.path.dirname(os.path.dirname(__file__))+'/Banco/mydatabase.db'


    def executaQuery(self,codigo):
        try:


            con=sqlite3.connect(self.caminho)

            stm=con.cursor()
            rs=stm.execute(codigo)
            lista=rs.fetchall()


            stm.close()
            con.close()

            return lista
        except:
            raise ErroNaConsultaException("Algum Erro durante a consulta, verifique-a!")

    def executaUpdate(self,codigo):
        try:

            con=sqlite3.connect(self.caminho)

            stm=con.cursor()
            try:
                rs=stm.execute(codigo)
            except:

                raise DadoJaCadastradoException('Esse dado ja foi cadastrado!')
            #lista=rs.fetchall()

            con.commit()
            stm.close()
            con.close()


        except DadoJaCadastradoException as e:
            raise DadoJaCadastradoException('Esse dado ja foi cadastrado!')
        except:
            pass
    # def inserir(self,regras):
    #     for regra in regras:
    #         print "insert into regras(regra,fc) values(%s%s%s,%s%f%s)"%("'",regra.regra,"'","'",regra.fc,"'")
    #         #print regra
    #         self.executaUpdate("insert into regras(regra,fc) values(%s%s%s,%s%f%s)"%("'",regra.regra,"'","'",regra.fc,"'"))

    # def selecionar(self,codigo='select * from Regras;'):
    #     return self.executaQuery(codigo)

    # def deletar(self,regra):
    #     codigo="delete from regras where regra=%s%s%s"%("'",regra,"'")
    #     self.executaUpdate(codigo)

    # def busca_variavel(self,variavel):
    #     variavel=re.match(r'^(\w+)\s*=?\s*\w*$',variavel).group(1)
    #     try:
    #         return int(self.executaQuery("select tipo from variavel where nome=\'%s\'"%variavel)[0][0])
    #     except:
    #         return None
    #
    # def inserir_variavel(self,variavel,tipo):
    #
    #     print "insert into variavel(nome,tipo) values(%s%s%s,%s%f%s)"%("'",variavel,"'","'",tipo,"'")
    #     #print regra
    #     self.executaUpdate("insert into variavel(nome,tipo) values(%s%s%s,%s%f%s)"%("'",variavel,"'","'",tipo,"'"))


#
# banco = GerenciaBanco()
# print banco.selecionar()
#
# print banco.inserir_variavel('introducao',2)
#print banco.selecionar()
# banco.inserir([
#     Regra('se A entao DF=Uiii',80),
#         #Regra('se A entao L',50.0),
#
#         #Regra('se Z or J entao P',50.0),
#         #Regra('se J & W entao D',50.0),
#         #Regra('se L entao J',50.0)
#
#
# ])

#se exercicio & disciplina=engenharia entao livro=SummerVille
#print banco.busca_variavel('asdasd')




#print banco.selecionar()

#banco.deletar('Se a entao B')